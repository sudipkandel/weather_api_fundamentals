Case study assessment of the programming fundamentals course. 

The data source that you will have to use is from the OpenWeatherMap website (https://openweathermap.org/).
You can have a look at their API documentation page https://openweathermap.org/api which will provide you with the necessary information of how to make an API call. 
You will have to create a free account in order to make the API calls also please keep in mind the limitations of the free account https://openweathermap.org/appid

The code can be in either Java or Python you are free to choose between them. You are allowed to use any type of additional open source libraries if you feel you need it. 

The task is to create a redistributable package be it in Java or Python that will basically hit the weather API and fetch the records and
then generate a report that will provide a weather forecast for the next 5 days for any location given as an input. 
The report need not be of any specific file format (you can generate the report even as a .txt file) but it should be understandable. 
After this the code should be uploaded on Bitbucket platform. Please create a personal repository and push all the code along with the 
redistributable package that you have built in that repository and add Ankit and me to that repository so that we can check it. 

The last date for submission is 4th August EOD. 

NOTE - If the code is sent by using any other platform then you will not be clearing the assessment.

Following criteria, if met will be awarded bonus points:-
- You are required to call the API on a separate thread/process other than the main thread of your program while maintaining 
proper synchronization.
- Proper exception handling is done.
- Made a generalized approach that can be extended to basically any type of similar API calls.
- Code is well documented and readable

The assessment will be done on how best one illustrates the concepts and follows the best practices along with the usage of 
various in-built libraries and frameworks. 
Also please note that even if you can build a code that does the task it does not translate necessarily into clearing the assessment. 
As it has been previously mentioned the objective is to create a redistributable package that can be easily integrated into any project 
that utilizes a similar API service for fetching data. 

