from setuptools import setup

setup(name='weather_api',
      version='0.1',
      description='Package to get weather details',
      url='#',
      author='Sudip Kandel',
      author_email='sudip.kandel@bridgei2i.com',
      license='No License',
      packages=['weather_api'],
      zip_safe=False)
