"""All the imports here"""
import requests
import pandas as pd
# import json
from weather_api.recursive_json import extract_values
from weather_api.config import *


class WeatherReader:
    def get_response(self, location):
        """Get the response from the api using the location and api passed by the user"""
        try:  # Exception handling
            response = requests.get(
                f"http://api.openweathermap.org/data/2.5/forecast?q={location}&appid={API_KEY}")

            if response.status_code == 200:  # If successful response get inside the loop
                print("Successful Response with response code: \n ", str(response.status_code))
                print("Start")
                response.encoding = "utf-8"
                self.response_json = response.json()
                print(self.response_json)

            else:
                print("An error has occured with response code: \n", str(response.status_code))

        except Exception as e:
            print("Error occurred while loading the site with message: \n ", str(e))

    def get_json_object(self, location):
        """Gets the json object for the specific fields"""
        self.get_response(location)  # Call the functions

        """Selected KPI's"""
        main = extract_values(self.response_json, 'main')
        description = extract_values(self.response_json, 'description')
        date = extract_values(self.response_json, 'dt_txt')
        wind_speed = extract_values(self.response_json, 'speed')
        current_temperature = extract_values(self.response_json, 'temp')
        feels_like = extract_values(self.response_json, 'feels_like')
        pressure = extract_values(self.response_json, 'pressure')
        humidity = extract_values(self.response_json, 'humidity')
        min_temperature = extract_values(self.response_json, 'temp_min')
        max_temperature = extract_values(self.response_json, 'temp_max')

        """Get it inside a list"""
        string_list = [date, main, description, wind_speed, current_temperature, feels_like, pressure,
                          humidity, min_temperature, max_temperature]
        columns = ["date", "main", "description",  "wind_speed", "current_temperature", "feels_like", "pressure",
                   "humidity", "min_temperature", "max_temperature"]

        """Dictionary object to create the DataFrame"""
        data_dict = dict(zip(columns, string_list))

        """creating the dataFrame"""
        df = pd.DataFrame(data_dict, columns=columns)
        df["Location"] = location

        """Export the the Desired Location"""
        df.to_csv(csv_output_path)  # Configure Path in config.py file
        print("End")


"""Object Creation and invokes"""
# WeatherObj = WeatherReader()
# print(WeatherObj.get_json_object("bangalore"))
